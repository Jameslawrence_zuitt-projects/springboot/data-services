package com.villanueva.s01.model;

import com.fasterxml.jackson.annotation.JsonTypeId;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="posts")
public class Post {
    //properties
        //id
        @Id
        @GeneratedValue
        private Long id;
        //title
        @Column
        private String title;
        //content
        @Column
        private String content;

    //contructor
    public Post(){}
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    //getters and setters
    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(){
        this.content = content;
    }
}
